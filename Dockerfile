FROM maven:3.5-jdk-8 as builder

ARG HTTP_PROXY=""
ARG HTTP_PROXY_HOST=""
ARG HTTP_PROXY_PORT=""
ARG TAG_VERSION="1.0.0"
ARG TAG_BUILD="000"
ARG TAG_COMMIT="NOT DEFINED"
ARG BUILD_DATE="NOT DEFINED"
ARG artifactory_username=""
ARG artifactory_apikey=""
ARG version_pipeline="not define"
ENV ARTIFACTORY_USER=$artifactory_username
ENV ARTIFACTORY_APIKEY=$artifactory_apikey


WORKDIR /usr/src

COPY . .

RUN HTTP_PROXY_HOST=$(echo $http_proxy | sed -E "s/^(http?:\/\/)?([^:]+):([0-9]+)/\2/")
RUN HTTP_PROXY_PORT=$(echo $http_proxy | sed -E "s/^(http?:\/\/)?([^:]+):([0-9]+)/\3/")
RUN mvn -Dhttp.proxyHost=${HTTP_PROXY_HOST} -Dhttp.proxyPort=${HTTP_PROXY_PORT} \
        -Dhttps.proxyHost=${HTTP_PROXY_HOST} -Dhttps.proxyPort=${HTTP_PROXY_PORT}  \
        -s settings.xml package -DskipTests
RUN cp target/marguerite-*.jar target/marguerite.jar

FROM openjdk:8-jre-alpine

LABEL sia="dsb"
LABEL irn="70295"
LABEL maintainer="aitslab@renaul-digital.com"
LABEL commit="${TAG_COMMIT}"
LABEL version="${TAG_VERSION}"
LABEL path_dockerfile "/home/Dockerfile"

ENV TERM=xterm
ENV SIA="dsb"
ENV IRN="70295"
ENV APP_NAME="dsb-api"
ENV TAG_COMMIT="${TAG_COMMIT}"
ENV BUILD_DATE="${BUILD_DATE}"
ENV TAG_VERSION="${TAG_VERSION}"
ENV TAG_BUILD="${TAG_BUILD}"


ENV JAVA_OPTS="-Xmx2048M"

COPY Dockerfile .

RUN apk update && apk add curl bash && rm -rf /var/cache/apk/*
RUN ln -sf /bin/bash /bin/sh
RUN addgroup -g 1000 user && adduser -u 1000 -G user -s /bin/bash -D user

WORKDIR /home/user
USER user


#COPY Dockerfile /Dockerfile
COPY --from=builder /usr/src/target/marguerite.jar .

EXPOSE 8080

ENTRYPOINT [ "sh", "-c", "java $JAVA_OPTS -jar /home/user/marguerite.jar" ]

HEALTHCHECK --interval=30s --timeout=3s CMD curl -f http://localhost:8080//api/management/health || exit 1
