package com.renault.dsb.mapper;

import com.renault.dsb.data.dto.UserDTO;
import com.renault.dsb.data.entity.User;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor"
)
@Component
public class UserMapperImpl implements UserMapper {

    @Override
    public UserDTO userToUserDTO(User user) {
        if ( user == null ) {
            return null;
        }

        UserDTO userDTO = new UserDTO();

        userDTO.setId( user.getId() );
        userDTO.setIpn( user.getIpn() );
        userDTO.setPassword( user.getPassword() );

        return userDTO;
    }

    @Override
    public User userDTOToUser(UserDTO userDTO) {
        if ( userDTO == null ) {
            return null;
        }

        User user = new User();

        user.setId( userDTO.getId() );
        user.setIpn( userDTO.getIpn() );
        user.setPassword( userDTO.getPassword() );

        return user;
    }

    @Override
    public List<UserDTO> usersToUsersDTO(List<User> users) {
        if ( users == null ) {
            return null;
        }

        List<UserDTO> list = new ArrayList<UserDTO>( users.size() );
        for ( User user : users ) {
            list.add( userToUserDTO( user ) );
        }

        return list;
    }

    @Override
    public List<User> usersDTOToUsers(List<UserDTO> usersDTO) {
        if ( usersDTO == null ) {
            return null;
        }

        List<User> list = new ArrayList<User>( usersDTO.size() );
        for ( UserDTO userDTO : usersDTO ) {
            list.add( userDTOToUser( userDTO ) );
        }

        return list;
    }
}
