package com.renault.dsb.mapper;

import com.renault.dsb.data.dto.SettingDTO;
import com.renault.dsb.data.entity.Setting;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor"
)
@Component
public class SettingMapperImpl implements SettingMapper {

    @Override
    public SettingDTO settingToSettingDTO(Setting setting) {
        if ( setting == null ) {
            return null;
        }

        SettingDTO settingDTO = new SettingDTO();

        settingDTO.setId( setting.getId() );
        settingDTO.setName( setting.getName() );
        settingDTO.setValue( setting.getValue() );
        settingDTO.setIsCrypt( setting.getIsCrypt() );

        return settingDTO;
    }

    @Override
    public Setting settingDTOToSetting(SettingDTO settingDTO) {
        if ( settingDTO == null ) {
            return null;
        }

        Setting setting = new Setting();

        setting.setId( settingDTO.getId() );
        setting.setName( settingDTO.getName() );
        setting.setValue( settingDTO.getValue() );
        setting.setIsCrypt( settingDTO.getIsCrypt() );

        return setting;
    }

    @Override
    public List<SettingDTO> settingsToSettingsDTO(List<Setting> settings) {
        if ( settings == null ) {
            return null;
        }

        List<SettingDTO> list = new ArrayList<SettingDTO>( settings.size() );
        for ( Setting setting : settings ) {
            list.add( settingToSettingDTO( setting ) );
        }

        return list;
    }

    @Override
    public List<Setting> settingsDTOToSettings(List<SettingDTO> settingsDTO) {
        if ( settingsDTO == null ) {
            return null;
        }

        List<Setting> list = new ArrayList<Setting>( settingsDTO.size() );
        for ( SettingDTO settingDTO : settingsDTO ) {
            list.add( settingDTOToSetting( settingDTO ) );
        }

        return list;
    }
}
