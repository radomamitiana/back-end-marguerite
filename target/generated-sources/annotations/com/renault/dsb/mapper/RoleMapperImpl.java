package com.renault.dsb.mapper;

import com.renault.dsb.data.dto.RoleDTO;
import com.renault.dsb.data.entity.Role;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor"
)
@Component
public class RoleMapperImpl implements RoleMapper {

    @Override
    public RoleDTO roleToRoleDTO(Role role) {
        if ( role == null ) {
            return null;
        }

        RoleDTO roleDTO = new RoleDTO();

        roleDTO.setId( role.getId() );
        roleDTO.setName( role.getName() );

        return roleDTO;
    }

    @Override
    public Role roleDTOToRole(RoleDTO roleDTO) {
        if ( roleDTO == null ) {
            return null;
        }

        Role role = new Role();

        role.setId( roleDTO.getId() );
        role.setName( roleDTO.getName() );

        return role;
    }

    @Override
    public List<RoleDTO> rolesToRolesDTO(List<Role> roles) {
        if ( roles == null ) {
            return null;
        }

        List<RoleDTO> list = new ArrayList<RoleDTO>( roles.size() );
        for ( Role role : roles ) {
            list.add( roleToRoleDTO( role ) );
        }

        return list;
    }

    @Override
    public List<Role> rolesDTOToRoles(List<RoleDTO> rolesDTO) {
        if ( rolesDTO == null ) {
            return null;
        }

        List<Role> list = new ArrayList<Role>( rolesDTO.size() );
        for ( RoleDTO roleDTO : rolesDTO ) {
            list.add( roleDTOToRole( roleDTO ) );
        }

        return list;
    }
}
