package com.renault.dsb.mapper;

import com.renault.dsb.data.dto.HistoryDTO;
import com.renault.dsb.data.entity.History;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor"
)
@Component
public class HistoryMapperImpl implements HistoryMapper {

    @Override
    public HistoryDTO historyToHistoryDTO(History history) {
        if ( history == null ) {
            return null;
        }

        HistoryDTO historyDTO = new HistoryDTO();

        historyDTO.setId( history.getId() );
        historyDTO.setIpn( history.getIpn() );
        historyDTO.setAction( history.getAction() );
        historyDTO.setState( history.getState() );
        historyDTO.setDate( history.getDate() );
        historyDTO.setSizeAddLines( history.getSizeAddLines() );
        historyDTO.setErrorReport( history.getErrorReport() );
        historyDTO.setHasError( history.isHasError() );

        return historyDTO;
    }

    @Override
    public History historyDTOToHistory(HistoryDTO historyDTO) {
        if ( historyDTO == null ) {
            return null;
        }

        History history = new History();

        history.setId( historyDTO.getId() );
        history.setIpn( historyDTO.getIpn() );
        history.setAction( historyDTO.getAction() );
        history.setState( historyDTO.getState() );
        history.setDate( historyDTO.getDate() );
        history.setSizeAddLines( historyDTO.getSizeAddLines() );
        history.setErrorReport( historyDTO.getErrorReport() );
        history.setHasError( historyDTO.isHasError() );

        return history;
    }

    @Override
    public List<HistoryDTO> historysToHistorysDTO(List<History> historys) {
        if ( historys == null ) {
            return null;
        }

        List<HistoryDTO> list = new ArrayList<HistoryDTO>( historys.size() );
        for ( History history : historys ) {
            list.add( historyToHistoryDTO( history ) );
        }

        return list;
    }

    @Override
    public List<History> historysDTOToHistorys(List<HistoryDTO> historysDTO) {
        if ( historysDTO == null ) {
            return null;
        }

        List<History> list = new ArrayList<History>( historysDTO.size() );
        for ( HistoryDTO historyDTO : historysDTO ) {
            list.add( historyDTOToHistory( historyDTO ) );
        }

        return list;
    }
}
