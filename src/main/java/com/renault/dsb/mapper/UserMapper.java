package com.renault.dsb.mapper;

import java.util.List;

import com.renault.dsb.data.dto.UserDTO;
import com.renault.dsb.data.entity.User;

import org.mapstruct.Mapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

@Mapper
public interface UserMapper {
	UserDTO userToUserDTO(User user);
	User userDTOToUser(UserDTO userDTO);
	List <UserDTO> usersToUsersDTO(List<User> users);
	List <User> usersDTOToUsers(List<UserDTO> usersDTO);

	default Page<UserDTO> userPageToUserDtoPage(Page<User> userPage, Pageable pageable) {
		List<UserDTO> userDtos = usersToUsersDTO(userPage.getContent());
		Page<UserDTO> userDTOPage = new PageImpl<>(userDtos, pageable, userPage.getTotalElements());

		return userDTOPage;
	}
}