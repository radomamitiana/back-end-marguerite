package com.renault.dsb.mapper;

import com.renault.dsb.data.dto.HistoryDTO;
import com.renault.dsb.data.entity.History;
import org.mapstruct.Mapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.util.List;

@Mapper
public interface HistoryMapper {
	HistoryDTO historyToHistoryDTO(History history);

	History historyDTOToHistory(HistoryDTO historyDTO);

	List<HistoryDTO> historysToHistorysDTO(List<History> historys);

	List<History> historysDTOToHistorys(List<HistoryDTO> historysDTO);

	default Page<HistoryDTO> historyPageToHistoryDtoPage(Page<History> historyPage, Pageable pageable) {
		List<HistoryDTO> historyDtos = historysToHistorysDTO(historyPage.getContent());
		Page<HistoryDTO> historyDTOPage = new PageImpl<>(historyDtos, pageable, historyPage.getTotalElements());

		return historyDTOPage;
	}

}
