package com.renault.dsb.service.dslpr;

import com.renault.dsb.common.config.security.services.UserPrinciple;
import com.renault.dsb.data.dto.HistoryDTO;
import com.renault.dsb.data.dto.ResponseDTO;
import com.renault.dsb.service.history.HistoryService;
import com.renault.dsb.service.utils.UtilService;

import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.StringTokenizer;

@Service
public class DslprServiceImpl implements DslprService, UtilService {

	private final static Logger logger = LoggerFactory.getLogger(DslprService.class);

	@Autowired
	private HistoryService historyService;

	/**
	 * download file from datalake to local
	 *
	 * @return
	 */
	@Override
	public Resource downloadCauseAleDeviation(String ipn, String password, String path) {
		clearAll();
		Runtime runtime = Runtime.getRuntime();
		ResponseDTO responseDTO = new ResponseDTO();

		StringBuilder commandLineBuilder = new StringBuilder();
		commandLineBuilder.append("curl -L -k -u ");
		commandLineBuilder.append(ipn);
		commandLineBuilder.append(":");
		commandLineBuilder.append(password);
		commandLineBuilder.append(" -X ");
		commandLineBuilder.append("GET ");
		commandLineBuilder.append(path);
		commandLineBuilder.append("?op=OPEN");

		Process process = null;
		try {
			process = runtime.exec(commandLineBuilder.toString());

			String line;
			BufferedReader output = new BufferedReader(new InputStreamReader(process.getInputStream()));

			BufferedWriter writer = new BufferedWriter(new FileWriter("storage/cause_ale_deviation.csv"));

			while ((line = output.readLine()) != null) {

				writer.write(line + "\n");
			}

			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		process.destroy();
		responseDTO.setOK(true);
		logger.info("Download successfull");

		HistoryDTO historyDTO = new HistoryDTO();
		UserPrinciple user = (UserPrinciple) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if (user != null)
			historyDTO.setIpn(user.getIpn());
		historyDTO.setAction("Download");
		historyDTO.setDate(LocalDate.now());
		historyDTO.setState("OK");
		historyDTO.setHasError(false);
		historyService.save(historyDTO);

		return loadAsResource("cause_ale_deviation.csv");
	}

	/**
	 * Upload file from local to the datalake
	 *
	 * @param ipn
	 * @return
	 */
	@Override
	public ResponseDTO uploadCauseAleDeviation(String ipn, String password, String path, MultipartFile file) {
		clearAll();
		Runtime runtime = Runtime.getRuntime();
		ResponseDTO responseDTO = new ResponseDTO();

		// Clear the file before uploading the same file
		store(file);
		boolean isOk = verify(file.getOriginalFilename());
		if (isOk) {
			// if the file control is OK, delete file on datalake and uplaod the new file
			clearData(runtime, ipn, password, path);
			try {
				StringBuilder commandLineBuilder = new StringBuilder();
				commandLineBuilder.append("curl -L -k -u ");
				commandLineBuilder.append(ipn);
				commandLineBuilder.append(":");
				commandLineBuilder.append(password);
				commandLineBuilder.append(" -X ");
				commandLineBuilder.append("PUT ");
				commandLineBuilder.append(path);
				commandLineBuilder.append("?op=create ");
				commandLineBuilder.append("-H 'Content-Type: applicaiton/text' -T ");
				commandLineBuilder.append("storage/" + file.getOriginalFilename());

				Process process = runtime.exec(commandLineBuilder.toString());

				String line;
				BufferedReader output = new BufferedReader(new InputStreamReader(process.getInputStream()));

				while ((line = output.readLine()) != null) {
					logger.info(line);
				}
				output.close();
				process.destroy();
				responseDTO.setOK(true);
				logger.info("Update successfull");

			} catch (IOException ex) {
				logger.error(ex.getMessage());
			}
		}
		return responseDTO;

	}

	/**
	 * download report file error
	 */
	@Override
	public Resource downloadErrorReport(String errorReport) {
		return loadAsResourceError(errorReport);
	}

	/**
	 * controle file before to send to the datalake
	 * 
	 * @param fileName
	 * @return
	 */
	private boolean verify(String fileName) {
		String extension = FilenameUtils.getExtension(fileName);
		int count = 1;
		boolean hasError = false;
		boolean hasSuccess = false;
		StringBuilder stringBuilderErrorFileName = new StringBuilder();

		// Control if is it a text file
		if (!StringUtils.isEmpty(extension) && (extension.equals("csv") || extension.equals("xls")
				|| extension.equals("xlsx") || extension.equals("txt"))) {
			File file = new File("storage/" + fileName);
			if (!file.exists())
				return false;

			try {
				FileReader fileReader = new FileReader("storage/" + fileName);
				BufferedReader bufferedReader = new BufferedReader(fileReader);

				int index = 2;
				int day = LocalDateTime.now().getDayOfMonth();
				int month = LocalDateTime.now().getMonthValue();
				int year = LocalDateTime.now().getYear();
				int hour = LocalDateTime.now().getHour();
				int minute = LocalDateTime.now().getMinute();
				int second = LocalDateTime.now().getSecond();

				stringBuilderErrorFileName.append(day);
				stringBuilderErrorFileName.append(month);
				stringBuilderErrorFileName.append(year);
				stringBuilderErrorFileName.append(hour);
				stringBuilderErrorFileName.append(minute);
				stringBuilderErrorFileName.append(second);
				stringBuilderErrorFileName.append(".txt");

				// Build the file error report
				BufferedWriter writer = new BufferedWriter(
						new FileWriter("file/" + stringBuilderErrorFileName.toString()));
				StringBuilder stringBuilderReportTitle = new StringBuilder();
				stringBuilderReportTitle.append("Action : Upload file\n\n");
				stringBuilderReportTitle.append("File name : ");
				stringBuilderReportTitle.append(fileName + "\n\n");
				stringBuilderReportTitle.append("Date : ");
				stringBuilderReportTitle.append(LocalDate.now().toString() + "\n\n");
				stringBuilderReportTitle.append("User : ");
				UserPrinciple user = (UserPrinciple) SecurityContextHolder.getContext().getAuthentication()
						.getPrincipal();
				if (user != null)
					stringBuilderReportTitle.append(user.getIpn());
				writer.write(stringBuilderReportTitle.toString() + "\n\n");
				writer.write("Result of upload : Errors found  - File rejected\n\n");
				writer.write("Errors :\n");
				String line = null;
				line = bufferedReader.readLine();
				StringTokenizer stringTokenizerFirstLine = new StringTokenizer(line, ";");

				// Control if the file contains 12 columns
				if (stringTokenizerFirstLine.countTokens() != 12) {
					writer.write("Line " + index + " : The number of column is not 12.\n");
					hasError = true;
				}

				// Control the column VIN
				while (line != null) {
					line = bufferedReader.readLine();
					if (line == null || StringUtils.isEmpty(line))
						break;
					StringTokenizer stringTokenizer = new StringTokenizer(line, ";");
					String vin = stringTokenizer.nextToken();
					if (line.charAt(0) == ';' || StringUtils.isEmpty(vin)) {
						writer.write("Line " + index + " : The column \"VIN\" is absent.\n");
						hasError = true;
					}

					// Control the column semaine
					String semaine = null;
					while (stringTokenizer.hasMoreTokens()) {
						semaine = stringTokenizer.nextToken();
					}
					if (StringUtils.isEmpty(semaine) || !semaine.matches("\\d+")) {
						writer.write("Line " + index + " : The column \"semaine\" is not an integer.\n");
						hasError = true;
					}

					// next line
					if (!hasError)
						hasSuccess = true;
					index++;
					count++;
				}
				writer.close();
				fileReader.close();
			} catch (IOException e) {
				System.err.println("Error -- " + e.toString());
			}
		}

		// Insert a new row in the history table
		HistoryDTO historyDTO = new HistoryDTO();
		UserPrinciple user = (UserPrinciple) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if (user != null)
			historyDTO.setIpn(user.getIpn());
		historyDTO.setAction("Upload");
		historyDTO.setDate(LocalDate.now());
		historyDTO.setSizeAddLines(count);
		if (!hasError && hasSuccess) {
			historyDTO.setHasError(false);
			historyDTO.setState("OK");

			// save history to the database
			historyService.save(historyDTO);
			removeFileReportTmp("file/" + stringBuilderErrorFileName.toString());
			return true;
		}
		historyDTO.setHasError(true);
		historyDTO.setErrorReport(stringBuilderErrorFileName.toString());
		historyDTO.setState("KO");

		// save history to the database
		historyService.save(historyDTO);
		return false;
	}
}
