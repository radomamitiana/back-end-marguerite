package com.renault.dsb.service.dslpr;

import com.renault.dsb.data.dto.ResponseDTO;
import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

public interface DslprService {

	/**
	 * Download file from locale to the datalake
	 * 
	 * @return
	 */
	Resource downloadCauseAleDeviation(String ipn, String password, String path);

	/**
	 * Upload file from local to the datalake
	 * 
	 * @param ipn
	 * @return
	 */
	ResponseDTO uploadCauseAleDeviation(String ipn, String password, String path, MultipartFile file);

	/**
	 * Download file error repport
	 * 
	 * @param errorReport
	 * @return
	 */
	Resource downloadErrorReport(String errorReport);
}
