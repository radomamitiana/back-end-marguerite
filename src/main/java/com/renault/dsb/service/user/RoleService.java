package com.renault.dsb.service.user;

import com.renault.dsb.data.dto.RoleDTO;
import com.renault.dsb.data.entity.RoleName;

public interface RoleService {
	RoleDTO createOrUpdate(RoleDTO roleDTO);

	RoleDTO findByName(RoleName roleName);

	void initRoles();
}
