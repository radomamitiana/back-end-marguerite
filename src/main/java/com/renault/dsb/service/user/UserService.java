package com.renault.dsb.service.user;

import com.renault.dsb.data.dto.AccountInformationDTO;
import com.renault.dsb.data.dto.IsDeletedDTO;
import com.renault.dsb.data.dto.UserDTO;
import com.renault.dsb.data.dto.UsersDTO;
import org.springframework.data.domain.Pageable;

public interface UserService {
	UserDTO create(UserDTO userDTO);

	UserDTO update(UserDTO userDTO);

	IsDeletedDTO deleteUser(Integer id);

	UsersDTO listUsers(Pageable pageable);

	UsersDTO listUsers(String stringQuery, Pageable pageable);

	UserDTO findById(Integer id);

	AccountInformationDTO findRoleByUser(Integer id);

	void initUserDefaultUser();

}
