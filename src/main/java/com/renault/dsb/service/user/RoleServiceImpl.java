package com.renault.dsb.service.user;

import com.renault.dsb.mapper.RoleMapper;
import com.renault.dsb.data.dto.RoleDTO;
import com.renault.dsb.data.entity.RoleName;
import com.renault.dsb.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RoleServiceImpl implements RoleService {

	@Autowired
	private RoleRepository roleRepository;

	@Autowired
	private RoleMapper roleFactory;

	@Override
	public RoleDTO createOrUpdate(RoleDTO roleDTO) {
		return roleFactory.roleToRoleDTO(roleRepository.saveAndFlush(roleFactory.roleDTOToRole(roleDTO)));
	}

	@Override
	public RoleDTO findByName(RoleName roleName) {
		return roleFactory.roleToRoleDTO(roleRepository.findByName(roleName).orElse(null));
	}

	@Override
	public void initRoles() {
		RoleDTO role = new RoleDTO();
		role.setId(1);
		role.setName(RoleName.ROLE_ADMIN);
		createOrUpdate(role);

		role = new RoleDTO();
		role.setId(2);
		role.setName(RoleName.ROLE_CO2);
		createOrUpdate(role);

		role = new RoleDTO();
		role.setId(3);
		role.setName(RoleName.ROLE_TOP_ALLIANCE);
		createOrUpdate(role);

		role = new RoleDTO();
		role.setId(4);
		role.setName(RoleName.ROLE_DSLPR);
		createOrUpdate(role);

		role = new RoleDTO();
		role.setId(5);
		role.setName(RoleName.ROLE_MONITORING);
		createOrUpdate(role);

	}

}
