package com.renault.dsb.service.history;

import com.renault.dsb.data.dto.HistoryDTO;
import com.renault.dsb.data.dto.HistorysDTO;
import org.springframework.data.domain.Pageable;

public interface HistoryService {
	HistorysDTO listHistorys(Pageable pageable);

	HistoryDTO save(HistoryDTO historyDTO);

	HistorysDTO listHistorys(String stringQuery, Pageable pageable);

}
