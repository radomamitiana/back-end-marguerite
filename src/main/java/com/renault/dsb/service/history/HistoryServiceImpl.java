package com.renault.dsb.service.history;

import com.renault.dsb.data.entity.History;
import com.renault.dsb.mapper.HistoryMapper;
import com.renault.dsb.data.dto.HistoryDTO;
import com.renault.dsb.data.dto.HistorysDTO;
import com.renault.dsb.repository.HistoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Comparator;
import java.util.List;
import java.util.function.BiPredicate;
import java.util.stream.Collectors;

@Service
public class HistoryServiceImpl implements HistoryService {

	@Autowired
	private HistoryRepository historyRepository;
	@Autowired
	private HistoryMapper historyFactory;

	/**
	 * get story list and filter by date
	 */
	@Override
	public HistorysDTO listHistorys(Pageable pageable) {
		HistorysDTO historysDTO = new HistorysDTO();
		Page<History> historyPage = historyRepository.findAll(pageable);
		Page<HistoryDTO> historyDTOPage = historyFactory
				.historyPageToHistoryDtoPage(historyPage, pageable);

		List<HistoryDTO> historyDTOList = historyDTOPage.getContent().stream()
				.sorted(Comparator.comparing(HistoryDTO::getDate).reversed()).collect(Collectors.toList());

		Page<HistoryDTO> historysDTOPage = new PageImpl<>(historyDTOList, historyPage.getPageable(), historyPage.getTotalElements());

		historysDTO.setHistoryDTOS(historysDTOPage);

		return historysDTO;
	}

	@Override
	public HistoryDTO save(HistoryDTO historyDTO) {
		return historyFactory
				.historyToHistoryDTO(historyRepository.save(historyFactory.historyDTOToHistory(historyDTO)));
	}

	/**
	 * Search anything in the story and filter by date
	 */
	@Override
	public HistorysDTO listHistorys(String stringQuery, Pageable pageable) {
		if (StringUtils.isEmpty(stringQuery)) {
			return listHistorys(pageable);
		}

		HistorysDTO historysDTO = new HistorysDTO();

		Page<History> historyPage = historyRepository.findAll(pageable);

		Page<HistoryDTO> historyDTOPage = historyFactory
				.historyPageToHistoryDtoPage(historyPage, pageable);
		BiPredicate<HistoryDTO, String> filterHistoryPredicate = (historyDTO,
				value) -> (!StringUtils.isEmpty(historyDTO.getIpn())
						&& historyDTO.getIpn().toLowerCase().contains(value.toLowerCase()))
						|| (!StringUtils.isEmpty(historyDTO.getAction())
								&& historyDTO.getAction().toLowerCase().contains(value.toLowerCase()))
						|| (!StringUtils.isEmpty(historyDTO.getState())
								&& historyDTO.getState().toLowerCase().contains(value.toLowerCase()));

		List<HistoryDTO> historyDTOList = historyDTOPage.getContent().stream()
				.filter(x -> filterHistoryPredicate.test(x, stringQuery))
				.sorted(Comparator.comparing(HistoryDTO::getDate).reversed()).collect(Collectors.toList());

		Page<HistoryDTO> historysDTOPage = new PageImpl<>(historyDTOList, historyPage.getPageable(), historyPage.getTotalElements());

		historysDTO.setHistoryDTOS(historysDTOPage);

		return historysDTO;
	}

}
