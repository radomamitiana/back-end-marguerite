package com.renault.dsb.data.dto.common;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class UnknownErrorDTO extends BaseDTO{

}
