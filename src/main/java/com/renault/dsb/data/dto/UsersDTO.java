package com.renault.dsb.data.dto;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.domain.Page;

import com.renault.dsb.data.dto.common.BaseDTO;

@Getter
@Setter
public class UsersDTO extends BaseDTO {
	Page<UserDTO> userDTOs;
}
