package com.renault.dsb.data.dto.common;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.renault.dsb.data.dto.UserDTO;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
@JsonSubTypes( @Type(value = UserDTO.class, name = "userDTO"))
public abstract class BaseDTO {

	@JsonInclude(JsonInclude.Include.NON_NULL)
	private boolean isError = false;

	@JsonInclude(JsonInclude.Include.NON_NULL)
	private boolean isInfo = false;

	@JsonInclude(JsonInclude.Include.NON_NULL)
	private boolean isWarning = false;

	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String uuid;

	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String errorCode = null;

	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String errorMessage = null;

	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String message = null;

	public BaseDTO(boolean isError, String errorCode, String errorMessage, String message) {
		super();
		this.isError = isError;
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
		this.message = message;
	}

	public BaseDTO(String uuid, String errorMessage, String message) {
		this.uuid = uuid;
		this.errorMessage = errorMessage;
		this.message = message;
	}

}
