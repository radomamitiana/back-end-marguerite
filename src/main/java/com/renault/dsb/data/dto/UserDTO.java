package com.renault.dsb.data.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.renault.dsb.data.dto.common.BaseDTO;

import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.Set;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class UserDTO extends BaseDTO {
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Integer id;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String ipn;

    @JsonIgnore
    private String password;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Set<String> strRoles;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private DropdownListDTO dropdownListDTO;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private List<DropdownDataDTO> dropdownDataDTOS;
}
