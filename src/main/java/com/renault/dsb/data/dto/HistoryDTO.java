package com.renault.dsb.data.dto;

import java.time.LocalDate;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HistoryDTO {
	private Integer id;
	private String ipn;
	private String action;
	private String state;
	private LocalDate date;
	private int sizeAddLines;
	private String errorReport;
	private boolean hasError;

}
