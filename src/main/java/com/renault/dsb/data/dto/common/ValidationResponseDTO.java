package com.renault.dsb.data.dto.common;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class ValidationResponseDTO {

	private Map<String, String> mapErrors;
	private Boolean isError;
}
