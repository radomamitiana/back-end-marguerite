package com.renault.dsb.data.dto;

import java.util.Set;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.renault.dsb.data.dto.common.BaseDTO;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class UserAuthDTO extends BaseDTO {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String ipn;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Set<String> roles;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String password;

}
