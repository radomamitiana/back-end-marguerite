package com.renault.dsb.data.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.renault.dsb.data.dto.common.BaseDTO;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class AccountInformationDTO extends BaseDTO {
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private List<String> roles;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Boolean isAdmin = false;
}
