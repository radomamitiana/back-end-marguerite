package com.renault.dsb.data.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.renault.dsb.data.dto.common.BaseDTO;
import com.renault.dsb.data.entity.RoleName;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class RoleDTO extends BaseDTO {
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Integer id;
    @JsonIgnore
    private RoleName name;
}
