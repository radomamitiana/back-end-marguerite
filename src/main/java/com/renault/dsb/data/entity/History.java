package com.renault.dsb.data.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;

@Getter
@Setter
@Entity
@Table(name = "history")
public class History {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	private String ipn;
	private String action;
	private String state;
	private LocalDate date;
	private int sizeAddLines;
	private String errorReport;
	private boolean hasError;
}
