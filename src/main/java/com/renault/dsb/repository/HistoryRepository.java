package com.renault.dsb.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.renault.dsb.data.entity.History;

public interface HistoryRepository extends JpaRepository<History, Integer> {

}
