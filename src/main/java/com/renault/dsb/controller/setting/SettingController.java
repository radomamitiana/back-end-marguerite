package com.renault.dsb.controller.setting;

import com.renault.dsb.common.utils.PermissionsAndStatusUtils;
import com.renault.dsb.data.dto.IsDeletedDTO;
import com.renault.dsb.data.dto.SettingDTO;
import com.renault.dsb.data.dto.SettingsDTO;
import com.renault.dsb.service.setting.SettingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping(value = "/api/setting")
public class SettingController {

	@Autowired
	private SettingService settingService;

	@PreAuthorize(PermissionsAndStatusUtils.ROLE_ADMIN)
	@PostMapping(value = { "/createOrUpdate" }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public SettingDTO createOrUpdate(@Valid @RequestBody SettingDTO settingDTO) {

		return settingService.createOrUpdate(settingDTO);
	}

	@PreAuthorize(PermissionsAndStatusUtils.ROLE_ADMIN)
	@DeleteMapping(path = "/delete", params = { "id" }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public IsDeletedDTO delete(@RequestParam(value = "id") Integer id) {
		return settingService.delete(id);
	}

	@PreAuthorize(PermissionsAndStatusUtils.ROLE_ADMIN)
	@GetMapping(value = { "/list" }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public SettingsDTO listSettings(@RequestParam(value = "page", defaultValue = "0", required = false) int page,
			@RequestParam(value = "size", defaultValue = "5", required = false) int size) {
		return settingService.listSettings(PageRequest.of(page, size));
	}

	/**
	 * Search something in the setting list
	 * 
	 * @param stringQuery
	 * @param pageable
	 * @return
	 */
	@PreAuthorize(PermissionsAndStatusUtils.ROLE_ADMIN)
	@GetMapping(value = { "/list" }, params = { "stringQuery" }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public SettingsDTO searchSettings(@RequestParam(value = "stringQuery", required = false) String stringQuery,
			@RequestParam(value = "page", defaultValue = "0", required = false) int page,
			@RequestParam(value = "size", defaultValue = "5", required = false) int size) {
		return settingService.listSettings(stringQuery, PageRequest.of(page, size));
	}

}
