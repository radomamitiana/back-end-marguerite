package com.renault.dsb.controller.auth;

import com.renault.dsb.common.config.security.services.UserPrinciple;
import com.renault.dsb.common.utils.PermissionsAndStatusUtils;
import com.renault.dsb.data.dto.AccountInformationDTO;
import com.renault.dsb.service.user.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping(value = "/api/account")
public class AccountController {
	@Autowired
	private UserService userService;

	@PreAuthorize(PermissionsAndStatusUtils.ROLE_USER)
	@GetMapping(path = "/get")
	public AccountInformationDTO getAccountInformation() {
		UserPrinciple user = (UserPrinciple) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		return userService.findRoleByUser(user.getId());
	}

}
