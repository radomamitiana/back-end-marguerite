package com.renault.dsb.controller.user;

import com.renault.dsb.common.utils.PermissionsAndStatusUtils;
import com.renault.dsb.data.dto.IsDeletedDTO;
import com.renault.dsb.data.dto.UserDTO;
import com.renault.dsb.data.dto.UsersDTO;
import com.renault.dsb.service.user.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping(value = "/api/user")
public class UserController {
	@Autowired
	private UserService userService;

	@PreAuthorize(PermissionsAndStatusUtils.ROLE_ADMIN)
	@PostMapping(value = { "/add" }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public UserDTO saveUser(@Valid @RequestBody UserDTO userDTO) {
		return userService.create(userDTO);

	}

	@PreAuthorize(PermissionsAndStatusUtils.ROLE_ADMIN)
	@PutMapping(path = "/update")
	public UserDTO updateUser(@RequestBody UserDTO userDTO) {

		return userService.update(userDTO);
	}

	@PreAuthorize(PermissionsAndStatusUtils.ROLE_ADMIN)
	@DeleteMapping(path = "/delete", params = { "id" }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public IsDeletedDTO deleteUser(@RequestParam(value = "id") Integer id) {
		return userService.deleteUser(id);
	}

	@PreAuthorize(PermissionsAndStatusUtils.ROLE_ADMIN)
	@GetMapping(value = { "/list" }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public UsersDTO listUsers(
			@RequestParam(value = "page", defaultValue = "0", required = false) int page,
			@RequestParam(value = "size", defaultValue = "5", required = false) int size) {
		return userService.listUsers(PageRequest.of(page, size));
	}

	@PreAuthorize(PermissionsAndStatusUtils.ROLE_ADMIN)
	@GetMapping(path = "/{id}/get")
	public UserDTO getUser(@PathVariable("id") Integer id) {
		return userService.findById(id);
	}

	/**
	 * search user endpoint
	 * 
	 * @param stringQuery
	 * @param pageable
	 * @return
	 */
	@PreAuthorize(PermissionsAndStatusUtils.ROLE_ADMIN)
	@GetMapping(value = { "/list" }, params = { "stringQuery" }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public UsersDTO searchUsers(@RequestParam(value = "stringQuery", required = false) String stringQuery,
			@RequestParam(value = "page", defaultValue = "0", required = false) int page,
			@RequestParam(value = "size", defaultValue = "5", required = false) int size) {
		return userService.listUsers(stringQuery, PageRequest.of(page, size));
	}
}
