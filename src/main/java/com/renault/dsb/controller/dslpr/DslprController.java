package com.renault.dsb.controller.dslpr;

import com.renault.dsb.common.utils.PermissionsAndStatusUtils;
import com.renault.dsb.data.dto.ResponseDTO;
import com.renault.dsb.data.dto.SettingDTO;
import com.renault.dsb.service.dslpr.DslprService;

import com.renault.dsb.service.setting.SettingService;
import com.renault.dsb.service.utils.CryptoUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping(value = "/api/dslpr")
public class DslprController {

	@Autowired
	private DslprService dslprService;

	@Autowired
	private SettingService settingService;

	/**
	 * download cause ALE Deviation Endpoint
	 * 
	 * @return ResponseDTO
	 */
	@PreAuthorize(PermissionsAndStatusUtils.ROLE_DSLPR)
	@GetMapping(path = "/downloadCauseAleDeviation")
	public ResponseEntity<Resource> download() {
		CryptoUtil cryptoUtil = new CryptoUtil();
		SettingDTO settingIpn = settingService.getValueByName("hadoop.dsb.ipn");
		SettingDTO settingPassword = settingService.getValueByName("hadoop.dsb.password");
		SettingDTO settingPath = settingService.getValueByName("hadoop.dsb.dslpr.caledev.path");
		if (settingIpn != null && !StringUtils.isEmpty(settingIpn.getValue()) && settingPassword != null
				&& !StringUtils.isEmpty(settingPassword.getValue()) && settingPath != null
				&& !StringUtils.isEmpty(settingPath.getValue())) {
			Resource file = dslprService.downloadCauseAleDeviation(settingIpn.getValue(), cryptoUtil.decrypt(settingPassword.getValue()),
					settingPath.getValue());
			return ResponseEntity.ok()
					.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"")
					.body(file);
		}
		return null;
	}

	/**
	 * upload cause ALE Deviation Endpoint
	 * 
	 * @return ResponseDTO
	 */
	@PreAuthorize(PermissionsAndStatusUtils.ROLE_DSLPR)
	@PostMapping(value = { "/uploadCauseAleDeviation" })
	public ResponseDTO upload(@RequestParam("file") MultipartFile multipartFile) {
		CryptoUtil cryptoUtil = new CryptoUtil();
		SettingDTO settingIpn = settingService.getValueByName("hadoop.dsb.ipn");
		SettingDTO settingPassword = settingService.getValueByName("hadoop.dsb.password");
		SettingDTO settingPath = settingService.getValueByName("hadoop.dsb.dslpr.caledev.path");
		if (settingIpn != null && !StringUtils.isEmpty(settingIpn.getValue()) && settingPassword != null
				&& !StringUtils.isEmpty(settingPassword.getValue()) && settingPath != null
				&& !StringUtils.isEmpty(settingPath.getValue())) {
			return dslprService.uploadCauseAleDeviation(settingIpn.getValue(), cryptoUtil.decrypt(settingPassword.getValue()),
					settingPath.getValue(), multipartFile);
		}
		return null;
	}

	/**
	 * Download error report endpoint
	 * 
	 * @return file
	 */
	@PreAuthorize(PermissionsAndStatusUtils.ROLE_DSLPR)
	@GetMapping(path = "/downloadErrorReport/{errorReport}")
	public ResponseEntity<Resource> downloadErrorReport(@PathVariable("errorReport") String errorReport) {
		Resource file = dslprService.downloadErrorReport(errorReport);
		return ResponseEntity.ok()
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"")
				.body(file);
	}

}
