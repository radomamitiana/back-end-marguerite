package com.renault.dsb.controller.history;

import com.renault.dsb.common.utils.PermissionsAndStatusUtils;
import com.renault.dsb.data.dto.HistorysDTO;
import com.renault.dsb.service.history.HistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping(value = "/api/history")
public class HistoryController {

	@Autowired
	private HistoryService historyService;

	/**
	 * get history list
	 * @param page
	 * @param size
	 * @return
	 */
	@PreAuthorize(PermissionsAndStatusUtils.ROLE_DSLPR)
	@GetMapping(value = { "/list" }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public HistorysDTO listHistorys(@RequestParam(value = "page", defaultValue = "0", required = false) int page,
			@RequestParam(value = "size", defaultValue = "3", required = false) int size) {
		return historyService.listHistorys(PageRequest.of(page, size));
	}

	/**
	 * search endpoint (search anything in the story table)
	 * @param stringQuery
	 * @param page
	 * @param size
	 * @return
	 */
	@PreAuthorize(PermissionsAndStatusUtils.ROLE_DSLPR)
	@GetMapping(value = { "/list" }, params = { "stringQuery" }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public HistorysDTO searchHistorys(@RequestParam(value = "stringQuery", required = false) String stringQuery,
			@RequestParam(value = "page", defaultValue = "0", required = false) int page,
			@RequestParam(value = "size", defaultValue = "3", required = false) int size) {
		return historyService.listHistorys(stringQuery, PageRequest.of(page, size));
	}

}
