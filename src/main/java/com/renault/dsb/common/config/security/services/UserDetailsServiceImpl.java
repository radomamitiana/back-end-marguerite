package com.renault.dsb.common.config.security.services;

import com.renault.dsb.data.entity.User;

import com.renault.dsb.repository.UserRepository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

	@Autowired
	UserRepository userRSM;

	@Override
	@Transactional
	public UserDetails loadUserByUsername(String ipn) throws UsernameNotFoundException {
		List<User> users = userRSM.findByIpn(ipn);
		User user = (users.size() > 0) ? users.get(0) : null;
		if (user == null) {
			throw new UsernameNotFoundException("User Not Found with -> username or email : " + ipn);
		}
		return UserPrinciple.build(user);
	}
}