package com.renault.dsb.common.utils;

public interface PermissionsAndStatusUtils {

    String ROLE_ADMIN = "hasRole('ADMIN')";

    String ROLE_CO2 = "hasRole('CO2') or hasRole('ADMIN')";

    String ROLE_TOP_ALLIANCE = "hasRole('TOP_ALLIANCE') or hasRole('ADMIN')";

    String ROLE_DSLPR = "hasRole('DSLPR') or hasRole('ADMIN')";

    String ROLE_MONITORING = "hasRole('MONITORING') or hasRole('ADMIN')";

    String ROLE_USER = "hasRole('ADMIN') or hasRole('CO2') or hasRole('TOP_ALLIANCE') or hasRole('DSLPR') or hasRole('MONITORING')";
}
